package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;
    private ArrayList<Flower> flowerList = new ArrayList<>();

    public DOMController(String xmlFileName) throws ParserConfigurationException, IOException, SAXException {
        this.xmlFileName = xmlFileName;
    }
	private static String getTagValue(String tag, Element element) {
		NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
		Node node = (Node) nodeList.item(0);
		return node.getNodeValue();
	}
	private static String getAttributeValue(String tag,String attribute,Element element) {
		Node node = element.getElementsByTagName(tag).item(0);
		Element element1=(Element) node;
		return element1.getAttribute(attribute);
	}
    public void DOMParse() throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(xmlFileName);
        NodeList flowers = document.getDocumentElement().getElementsByTagName("flower");

        for (int i = 0; i < flowers.getLength(); i++) {
            Node flower = flowers.item(i);

			Element element=(Element) flower;



            flowerList.add(new Flower(getTagValue("name",element)
					,getTagValue("soil",element)
					,getTagValue("origin",element)
					,getTagValue("stemColour",element)
					,getTagValue("leafColour",element)
					,Integer.parseInt(getTagValue("aveLenFlower",element))
					,Integer.parseInt(getTagValue("tempreture",element))
					,getAttributeValue("lighting","lightRequiring",element)
					,Integer.parseInt(getTagValue("watering",element))
					,getTagValue("multiplying",element)));
        }


    }

	public void DOMSort(){
		flowerList.sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				int avup = o1.getName().compareTo(o2.getName());
				return avup;
			}
		});
	}


	public void DOMFormOutput(String FileName) throws IOException, TransformerException, ParserConfigurationException {


		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
		Document doc = docBuilder.newDocument();
		Element trueRootElement = doc.createElement("flowers");
		trueRootElement.setAttribute("xmlns","http://www.nure.ua");
		trueRootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		trueRootElement.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd ");
		doc.appendChild(trueRootElement);


		for (Flower f:flowerList) {
			Element rootElement = doc.createElement("flower");
			trueRootElement.appendChild(rootElement);

			Element name=doc.createElement("name");
			name.setTextContent(f.getName());

			rootElement.appendChild(name);

			Element soil=doc.createElement("soil");
			soil.setTextContent(f.getSoil());
			rootElement.appendChild(soil);

			Element origin=doc.createElement("origin");
			origin.setTextContent(f.getOrigin());
			rootElement.appendChild(origin);

			Element visualParameters= doc.createElement("visualParameters");
			rootElement.appendChild(visualParameters);

			Element stemColour= doc.createElement("stemColour");
			stemColour.setTextContent(f.getStemColour());
			visualParameters.appendChild(stemColour);


			Element leafColour= doc.createElement("leafColour");
			leafColour.setTextContent(f.getLeafColour());
			visualParameters.appendChild(leafColour);


			Element aveLenFlower= doc.createElement("aveLenFlower");
			aveLenFlower.setAttribute("measure","cm");
			aveLenFlower.setTextContent(String.valueOf(f.getAveLenFlower()));
			visualParameters.appendChild(aveLenFlower);

			Element growingTips = doc.createElement("growingTips");
			rootElement.appendChild(growingTips);

			Element tempreture= doc.createElement("tempreture");
			tempreture.setAttribute("measure","celcius");
			tempreture.setTextContent(String.valueOf(f.getTempreture()));
			growingTips.appendChild(tempreture);

			Element lighting = doc.createElement("lighting");
			lighting.setAttribute("lightRequiring",f.getLighting());
			growingTips.appendChild(lighting);

			Element watering = doc.createElement("watering");
			watering.setAttribute("measure","mlPerWeek");
			watering.setTextContent(String.valueOf(f.getWatering()));
			growingTips.appendChild(watering);

			Element multiplying = doc.createElement("multiplying");
			multiplying.setTextContent(f.getMultiplying());
			rootElement.appendChild(multiplying);


		}


		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(FileName));



		transformer.transform(source, result);




	}
}

