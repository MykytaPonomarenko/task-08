package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	private ArrayList<Flower> flowerList = new ArrayList<>();

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public void SAXParse() throws ParserConfigurationException, SAXException, IOException {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		MySAX mySAX = new MySAX();
		saxParser.parse(xmlFileName,mySAX);
		System.out.println(flowerList);


	}

	public void SAXSort(){
		flowerList.sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				int avup = o1.getSoil().compareTo(o2.getSoil());
				return avup;
			}
		});
	}

	public void SAXFormOutput(String FileName) throws IOException, TransformerException, ParserConfigurationException {


		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
		Document doc = docBuilder.newDocument();
		Element trueRootElement = doc.createElement("flowers");
		trueRootElement.setAttribute("xmlns","http://www.nure.ua");
		trueRootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		trueRootElement.setAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd ");
		doc.appendChild(trueRootElement);


		for (Flower f:flowerList) {
			Element rootElement = doc.createElement("flower");
			trueRootElement.appendChild(rootElement);

			Element name=doc.createElement("name");
			name.setTextContent(f.getName());

			rootElement.appendChild(name);

			Element soil=doc.createElement("soil");
			soil.setTextContent(f.getSoil());
			rootElement.appendChild(soil);

			Element origin=doc.createElement("origin");
			origin.setTextContent(f.getOrigin());
			rootElement.appendChild(origin);

			Element visualParameters= doc.createElement("visualParameters");
			rootElement.appendChild(visualParameters);

			Element stemColour= doc.createElement("stemColour");
			stemColour.setTextContent(f.getStemColour());
			visualParameters.appendChild(stemColour);


			Element leafColour= doc.createElement("leafColour");
			leafColour.setTextContent(f.getLeafColour());
			visualParameters.appendChild(leafColour);


			Element aveLenFlower= doc.createElement("aveLenFlower");
			aveLenFlower.setAttribute("measure","cm");
			aveLenFlower.setTextContent(String.valueOf(f.getAveLenFlower()));
			visualParameters.appendChild(aveLenFlower);

			Element growingTips = doc.createElement("growingTips");
			rootElement.appendChild(growingTips);

			Element tempreture= doc.createElement("tempreture");
			tempreture.setAttribute("measure","celcius");
			tempreture.setTextContent(String.valueOf(f.getTempreture()));
			growingTips.appendChild(tempreture);

			Element lighting = doc.createElement("lighting");
			lighting.setAttribute("lightRequiring",f.getLighting());
			growingTips.appendChild(lighting);

			Element watering = doc.createElement("watering");
			watering.setAttribute("measure","mlPerWeek");
			watering.setTextContent(String.valueOf(f.getWatering()));
			growingTips.appendChild(watering);

			Element multiplying = doc.createElement("multiplying");
			multiplying.setTextContent(f.getMultiplying());
			rootElement.appendChild(multiplying);


		}


		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(FileName));



		transformer.transform(source, result);




	}


	public class MySAX extends DefaultHandler{
			String thisElement="";
			String attributes="";
			int counter = 0;
		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
			thisElement = qName;
			if (Objects.equals(thisElement, "lighting")){

				attributes=atts.getValue("lightRequiring");
				flowerList.get(counter-1).setLighting(attributes);

			}
		}

		@Override
		public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
			thisElement = "";
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			if (thisElement=="flower") {
				flowerList.add(new Flower());
				counter++;
			}
			if (thisElement=="name"){
				flowerList.get(counter-1).setName(new String(ch,start,length));
			}
			if (thisElement=="soil"){
				flowerList.get(counter-1).setSoil(new String(ch,start,length));
			}
			if (thisElement=="origin"){
				flowerList.get(counter-1).setOrigin(new String(ch,start,length));
			}
			if (thisElement=="stemColour"){
				flowerList.get(counter-1).setStemColour(new String(ch,start,length));
			}
			if (thisElement=="leafColour"){
				flowerList.get(counter-1).setLeafColour(new String(ch,start,length));
			}

			if (thisElement=="aveLenFlower"){
				flowerList.get(counter-1).setAveLenFlower(Integer.parseInt(new String(ch,start,length)));
			}
			if (thisElement=="tempreture"){
				flowerList.get(counter-1).setTempreture(Integer.parseInt(new String(ch,start,length)));
			}


			if (thisElement=="lighting"){
				flowerList.get(counter-1).setLighting(attributes);
			}
			if (thisElement=="watering"){
				flowerList.get(counter-1).setWatering(Integer.parseInt(new String(ch,start,length)));
			}
			if (thisElement=="multiplying"){
				flowerList.get(counter-1).setMultiplying(new String(ch,start,length));
			}





		}

}}