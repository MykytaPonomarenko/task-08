package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		domController.DOMParse();
		// sort (case 1)
		// PLACE YOUR CODE HERE
		domController.DOMSort();
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.DOMFormOutput(outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		saxController.SAXParse();
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		saxController.SAXSort();
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		saxController.SAXFormOutput(outputXmlFile);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		staxController.STAXParse();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		staxController.STAXSort();
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		staxController.STAXFormOutput(outputXmlFile);
	}

}
