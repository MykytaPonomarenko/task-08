package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;
    private ArrayList<Flower> flowerList = new ArrayList<>();

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    public void STAXParse() {
        Flower flower = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {

            XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

            while (reader.hasNext()) {

                XMLEvent xmlEvent = reader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();

                    if (startElement.getName().getLocalPart().equals("flower")) {
                        flower = new Flower();

                    } else if (startElement.getName().getLocalPart().equals("name")) {
                        xmlEvent = reader.nextEvent();
                        flower.setName(xmlEvent.asCharacters().getData());

                    } else if (startElement.getName().getLocalPart().equals("soil")) {
                        xmlEvent = reader.nextEvent();
                        flower.setSoil(xmlEvent.asCharacters().getData());

                    } else if (startElement.getName().getLocalPart().equals("origin")) {
                        xmlEvent = reader.nextEvent();
                        flower.setOrigin(xmlEvent.asCharacters().getData());

                    } else if (startElement.getName().getLocalPart().equals("stemColour")) {
                        xmlEvent = reader.nextEvent();
                        flower.setStemColour(xmlEvent.asCharacters().getData());

                    } else if (startElement.getName().getLocalPart().equals("leafColour")) {
                        xmlEvent = reader.nextEvent();
                        flower.setLeafColour(xmlEvent.asCharacters().getData());

                    } else if (startElement.getName().getLocalPart().equals("aveLenFlower")) {
                        xmlEvent = reader.nextEvent();
                        flower.setAveLenFlower(Integer.parseInt(xmlEvent.asCharacters().getData()));

                    } else if (startElement.getName().getLocalPart().equals("tempreture")) {
                        xmlEvent = reader.nextEvent();
                        flower.setTempreture(Integer.parseInt(xmlEvent.asCharacters().getData()));

                    } else if (startElement.getName().getLocalPart().equals("lighting")) {
                        Attribute idAttr = startElement.getAttributeByName(new QName("lightRequiring"));
                        if (idAttr != null) {
                            flower.setLighting(idAttr.getValue());
                        }
                        xmlEvent = reader.nextEvent();


                    } else if (startElement.getName().getLocalPart().equals("watering")) {
                        xmlEvent = reader.nextEvent();
                        flower.setWatering(Integer.parseInt(xmlEvent.asCharacters().getData()));

                    } else if (startElement.getName().getLocalPart().equals("multiplying")) {
                        xmlEvent = reader.nextEvent();
                        flower.setMultiplying(xmlEvent.asCharacters().getData());

                    }
                }

                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("flower")) {
                        flowerList.add(flower);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException exc) {
            exc.printStackTrace();
        }

    }

    public void STAXFormOutput(String FileName) throws IOException, TransformerException, ParserConfigurationException {


        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element trueRootElement = doc.createElement("flowers");
        trueRootElement.setAttribute("xmlns", "http://www.nure.ua");
        trueRootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        trueRootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
        doc.appendChild(trueRootElement);


        for (Flower f : flowerList) {
            Element rootElement = doc.createElement("flower");
            trueRootElement.appendChild(rootElement);

            Element name = doc.createElement("name");
            name.setTextContent(f.getName());

            rootElement.appendChild(name);

            Element soil = doc.createElement("soil");
            soil.setTextContent(f.getSoil());
            rootElement.appendChild(soil);

            Element origin = doc.createElement("origin");
            origin.setTextContent(f.getOrigin());
            rootElement.appendChild(origin);

            Element visualParameters = doc.createElement("visualParameters");
            rootElement.appendChild(visualParameters);

            Element stemColour = doc.createElement("stemColour");
            stemColour.setTextContent(f.getStemColour());
            visualParameters.appendChild(stemColour);


            Element leafColour = doc.createElement("leafColour");
            leafColour.setTextContent(f.getLeafColour());
            visualParameters.appendChild(leafColour);


            Element aveLenFlower = doc.createElement("aveLenFlower");
            aveLenFlower.setAttribute("measure", "cm");
            aveLenFlower.setTextContent(String.valueOf(f.getAveLenFlower()));
            visualParameters.appendChild(aveLenFlower);

            Element growingTips = doc.createElement("growingTips");
            rootElement.appendChild(growingTips);

            Element tempreture = doc.createElement("tempreture");
            tempreture.setAttribute("measure", "celcius");
            tempreture.setTextContent(String.valueOf(f.getTempreture()));
            growingTips.appendChild(tempreture);

            Element lighting = doc.createElement("lighting");
            lighting.setAttribute("lightRequiring", f.getLighting());
            growingTips.appendChild(lighting);

            Element watering = doc.createElement("watering");
            watering.setAttribute("measure", "mlPerWeek");
            watering.setTextContent(String.valueOf(f.getWatering()));
            growingTips.appendChild(watering);

            Element multiplying = doc.createElement("multiplying");
            multiplying.setTextContent(f.getMultiplying());
            rootElement.appendChild(multiplying);


        }


        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(FileName));


        transformer.transform(source, result);


    }

    public void STAXSort() {
        flowerList.sort(new Comparator<Flower>() {
            @Override
            public int compare(Flower o1, Flower o2) {
                int avup = o1.getOrigin().compareTo(o2.getOrigin());
                return avup;
            }
        });
    }

}